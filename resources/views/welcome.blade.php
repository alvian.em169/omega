<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <link rel="shortcut icon" href="assets/img/omega-logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keyword" content="olimpiade, ipa, um, universitas, smp, mts, malang, universitas negeri malang, omega, olimpiade ipa um">
    <meta name="description" content="Olimpiade IPA UM adalah bentuk partisipasi nyata dalam upaya
    meningkatkan kualitas pendidikan di negeri ini untuk menciptakan generasi yang cerdas dan
    memiliki jiwa kritis guna menuju Indonesia yang lebih baik.
">
    <meta name="author" content="Olimpiade.id">
    <meta property="og:url" content="index.html" />
    <meta property="og:image" content="assets/img/omega-logo.png" />
    <meta content='#fff' name='theme-color'/>
    <meta content='Indonesia' name='geo.placename'/>

    <title>Olimpiade IPA UM</title>

    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css'>

    <link href="assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="assets/css/freelancer-osd.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style1.css">
    <link rel="stylesheet" href="assets/css/aos.css">

    <script type="text/javascript"> (function() { var css = document.createElement('link'); css.href = 'http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css'; css.rel = 'stylesheet'; css.type = 'text/css'; document.getElementsByTagName('head')[0].appendChild(css); })(); </script>

    <style type="text/css">
      .caption-animate .carousel-item.active .carousel-caption {
        -webkit-animation-duration: 1s;
        animation-duration: 1s;
        -webkit-animation-fill-mode: both;
        animation-fill-mode: both;
      }
      .caption-animate  .carousel-item.active .carousel-caption.infinite {
        -webkit-animation-iteration-count: infinite;
        animation-iteration-count: infinite;
      }

      .caption-animate  .carousel-item.active .carousel-caption.hinge {
        -webkit-animation-duration: 2s;
        animation-duration: 2s;
      }

      .caption-animate .carousel-item.active .carousel-caption.flipOutX,
      .caption-animate .carousel-item.active .carousel-caption.flipOutY,
      .caption-animate .carousel-item.active .carousel-caption.bounceIn,
      .caption-animate .carousel-item.active .carousel-caption.bounceOut {
        -webkit-animation-duration: .75s;
        animation-duration: .75s;
      }
      .caption-animate .carousel-item.active .carousel-caption.fadeIn,
      .caption-animate .carousel-item.active .carousel-caption.fadeInDown,
      .caption-animate .carousel-item.active .carousel-caption.fadeInDownBig,
      .caption-animate .carousel-item.active .carousel-caption.fadeInLeft,
      .caption-animate .carousel-item.active .carousel-caption.fadeInLeftBig,
      .caption-animate .carousel-item.active .carousel-caption.fadeInRight,
      .caption-animate .carousel-item.active .carousel-caption.fadeInRightBig,
      .caption-animate .carousel-item.active .carousel-caption.fadeInUp,
      .caption-animate .carousel-item.active .carousel-caption.fadeInUpBig{
        opacity:0;
      }
    </style>

    <style type="text/css">
    #hilang{
      display: none;
    }
    #clockdiv{
    font-family: sans-serif;
    color: #fff;
    display: inline-block;
    font-weight: 100;
    text-align: center;
    font-size: 30px;
    }

    #clockdiv > div{
      padding: 10px;
      border-radius: 3px;
      background: #f1d02e;
      display: inline-block;
    }

    #clockdiv div > span{
      padding: 15px;
      border-radius: 3px;
      background: #008fd7;
      display: inline-block;
    }

    .smalltext{
      padding-top: 5px;
      font-size: 16px;
      font-weight: bold;
      font-family: Lato;
    }
    @media only screen and ( max-width : 769px ){
      #hilang{
        display: block;
      }
        #laptop{
          display: none;
        }
        #hp{
          display: block;
        }
        #my-slider {
          display: none;
        }
        #image_full{
         display:none;
        }

        #image_mobile{
         display:block;
        }
      }
    @media only screen and ( min-width : 769px ){
        #laptop{
          display: block;
        }
        #hp{
          display: none;
        }
        #my-slider2 {
          display: none;
        }
        #image_full{
           display:block;
          }

         #image_mobile{
          display:none;
         }
      }
    </style>

  </head>

  <body id="page-top">
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">OMEGA</a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#timeline">Timeline</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#prize">Prize</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
                <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#gallery">Gallery</a>
              </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#region">Region</a>
            </li>
            <li class="nav-item mx-0 mx-lg-1">
              <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#">Register</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <header class="masthead bg-primary text-white text-center" style="background: url('assets/img/bg-omega.jpg') no-repeat center center fixed;background-size: cover;">
      <div class="container">
        <center><h1 class="text-uppercase mb-0" style="color:#009b4c;">Olimpiade IPA</h1></center>
        <br>
        <h1 class="font-weight-light mb-0" style="color:#f1d02e;">2018</h1>
        <div class="text-center" style="padding-top: 25px">
          <a class="btn btn-xl btn-outline-light" href="http://ujian.osd-hmpipaum.com/" style="border-color:#ffffff;color:#ffffff;border-width: 6px;margin: 10px" id="buttons" download>
            <i class="fa fa-pencil-square-o mr-2" style="color:#ffffff"></i>
            Ujian <b> Penyisihan</b>!
          </a>
            <a class="btn btn-xl btn-outline-light" style="border-color:#ffffff;color:#ffffff;border-width: 6px;margin: 10px" id="buttons" href="http://pendaftaran.osd-hmpipaum.com/">
            <i class="fa fa-pencil-square-o mr-2" style="color:#ffffff"></i>
            Daftar <b>Disini</b>!
          </a>
          <a class="btn btn-xl btn-outline-light" href="assets/file/PETUNJUK PELAKSANAAN OLIMPIADE.pdf" style="border-color:#ffffff;color:#ffffff;border-width: 6px;margin: 10px" id="buttons" download>
            <i class="fa fa-pencil-square-o mr-2" style="color:#ffffff"></i>
            Download <b>Petunjuk</b>!
          </a>
        </div>
      </div>
    </header>

    <section class="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <center><h1 style="color:#4f4f4f">Penutupan Pendaftaran</h1>
              <div id="clockdiv">
                  <div>
                    <span class="days"></span>
                    <div class="smalltext">Days</div>
                  </div>
                  <div>
                    <span class="hours"></span>
                    <div class="smalltext">Hours</div>
                  </div>
                  <div>
                    <span class="minutes"></span>
                    <div class="smalltext">Minutes</div>
                  </div>
                  <div>
                    <span class="seconds"></span>
                    <div class="smalltext">Seconds</div>
                  </div>
                </div>
            
          </div></center>
          <div class="col-md-6 col-lg-6">
            <center><img src="assets/img/omega-logo.png" style="width: 50%" data-aos="flip-down"></center>
            <br>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-primary text-white mb-0" style="background-color: #009b4c!important" id="about">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">Apa itu Olimpiade IPA?</h2>
        <hr class="star-light mb-5">
        <div class="row">
          <div class="col-lg-4 ml-auto">
            <br>
            <center><img src="assets/img/maskot.png" style="width: 75%;text-align: center;padding-top: 75px"  data-aos="flip-up"></center>
            <br>
          </div>
          <div class="col-lg-7 ml-auto">
            <p class="lead" style="text-align: justify;">Diselenggarakan sebagai bentuk partisipasi nyata dalam upaya
              meningkatkan kualitas pendidikan di negeri ini untuk menciptakan generasi yang cerdas dan
              memiliki jiwa kritis guna menuju Indonesia yang lebih baik. <br><br>Dalam kegiatan ini, terdapat 4
              rangkaian agenda yang akan dilaksanakan, yaitu babak penyisihan, babak goes to semifinal,
              babak semifinal dan babak final Olimpiade IPA untuk SMP/MTs se-Jawa Bali. <br></br>Dengan adanya
              kegiatan ini, diharapkan adanya hubungan yang sinergis antara civitas akademik perguruan
              tinggi dengan masyarakat umum dalam berbagi ilmu dan pengetahuan.</p><br>
      <p class="lead" style="text-align: center;font-weight: bold">“Tema : Upgrade Your Critical Thinking Skill to be A Briliant Generation with Science Olympiad”</p>
          </div>
          <div class="col-lg-1 ml-auto">
          </div>
      </div>
    </section>

    <section class="portfolio" id="requirement">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Syarat & Ketentuan</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
          <div class="row">
            <div class="col-lg-12 ml-auto">
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• Peserta merupakan siswa/i SMP/MTs yang masih terdaftar sebagai siswa/i pada sekolah
                tertentu saat Olimpiade IPA SMP/MTs se-Jawa Bali 2018 berlangsung.</p>
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• Peserta bersifat individu.</p>
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• Peserta melakukan pendaftaran.</p>
              <p class="lead" style="text-align: left;" data-aos="zoom-out-down">• Untuk pelaksanaan offline, peserta wajib melakukan registrasi ulang saat pelaksanaan
                babak penyisihan di masing-masing rayon dengan membawa formulir pendaftaran atau
                bukti pendaftaran, fotokopi kartu tanda pelajar atau surat keterangan dari sekolah bagi
                yang belum memiliki, dan kuitansi atau bukti pembayaran.</p>
            </div>
          </div>
        </div>
      </section>

    <section class="bg-primary text-white mb-0" id="timeline" style="background-color: #008fd7!important">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">Timeline</h2>
        <hr class="star-light2 mb-5">
        <section id="cd-timeline" class="cd-container">
    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-picture" style="background-color: #f1d02e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#009b4c">Pendaftaran</h4>
        <p style="color:#4f4f4f" id="hilang"><i>11 Juni - 31 Juli 2018</i></p>
        <p style="color:#4f4f4f">Gelombang 1</p>
        <span class="cd-date" style="opacity: 1">11 Juni - 31 Juli 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f1d02e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#009b4c">Pendaftaran</h4>
        <p style="color:#4f4f4f" id="hilang"><i>1 Agustus - 15 September 2018</i></p>
        <p style="color:#4f4f4f">Gelombang 2</p>
        <span class="cd-date" style="opacity: 1">1 Agustus - 15 September 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f1d02e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#009b4c">Pendaftaran</h4>
        <p style="color:#4f4f4f" id="hilang"><i>16 September - 2 Oktober 2018</i></p>
        <p style="color:#4f4f4f">Gelombang 3</p>
        <span class="cd-date" style="opacity: 1">16 September - 2 Oktober 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f1d02e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#009b4c">Pendaftaran</h4>
        <p style="color:#4f4f4f" id="hilang"><i>7 Oktober 2018</i></p>
        <p style="color:#4f4f4f">On The Spot</p>
        <span class="cd-date" style="opacity: 1">7 Oktober 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-movie" style="background-color: #f1d02e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#009b4c">Penyisihan</h4>
        <p style="color:#4f4f4f" id="hilang"><i>7 Oktober 2018</i></p>
        <span class="cd-date" style="opacity: 1">7 Oktober 2018</span>
      </div>
    </div>

    <div class="cd-timeline-block">
      <div class="cd-timeline-img cd-picture" style="background-color: #f1d02e">
        <img src="assets/img/cd-icon-location.svg">
      </div>

      <div class="cd-timeline-content">
        <h4 style="color:#009b4c">Goes To Semifinal, Semifinal & Final</h4>
        <p style="color:#4f4f4f" id="hilang"><i>14 Oktober 2018</i></p>
        <span class="cd-date" style="opacity: 1">14 Oktober 2018</span>
      </div>
    </div>
  </section>
      </div>
    </section>

    <section class="portfolio" id="prize">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Hadiah</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
          <div class="row">
            <div class="col-md-6 col-lg-1" style="margin-top: 10px"></div>
            <div class="col-md-6 col-lg-2" style="margin-top: 10px">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara1.png" style="" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">Uang senilai Rp. <strong>2.000.000</strong>,<br>
            Trophy, dan Sertifikat
          </p>
            </div>
            <div class="col-md-6 col-lg-2" style="margin-top: 10px">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara2.png" style="width: 90%;padding-top: 10%" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">Uang senilai Rp. <strong>1.500.000</strong>,<br>
            Trophy, dan Sertifikat
          </p>
            </div>
            <div class="col-md-6 col-lg-2" style="margin-top: 10px">
               <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara3.png" style="width: 80%;padding-top: 20%" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">Uang senilai Rp. <strong>1.250.000</strong>,<br>
            Trophy, dan Sertifikat
          </p>
            </div>
            <div class="col-md-6 col-lg-2" style="margin-top: 10px">
               <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara4.png" style="width: 40%;padding-top: 55px" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">
            Trophy, dan Sertifikat
          </p>
            </div>
            <div class="col-md-6 col-lg-2" style="margin-top: 10px">
               <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/juara5.png" style="width: 37%;padding-top: 64px" data-aos="fade-up"></center>
              </a>
              <p style="text-align: center;">
            Trophy, dan Sertifikat
          </p>
            </div>
            <div class="col-md-6 col-lg-1" style="margin-top: 10px"></div>
          </div>
        </div>
      </section>

    <section class="bg-primary text-white mb-0" style="background-color: #f1d02e!important;">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#ffffff!important">Cara Pendaftaran</h2>
        <hr class="star-light3 mb-5" style="border-color:#ffffff">   <br>
        <h3 class="text-center text-uppercase text-secondary mb-0" style="color:#009b4c!important;padding-bottom: 10px">Online</h3>
        <section class="cd-horizontal-timeline" style="margin-top: 0px;margin-bottom: 0px;padding-top: 0px;padding-bottom: 0px" data-aos="fade-down">
  <div class="timeline">
    <div class="events-wrapper">
      <div class="events">
        <ol>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="16/01/2014" class="selected" style="color:#4f4f4f">1st</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="28/02/2014" style="color:#4f4f4f">2nd</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="20/04/2014" style="color:#4f4f4f">3rd</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="20/05/2014" style="color:#4f4f4f">4th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="09/07/2014" style="color:#4f4f4f">5th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="30/08/2014" style="color:#4f4f4f">6th</a></li>
          <li style="color: #f1d02e"><a href="index.html#0" data-date="15/09/2014" style="color:#4f4f4f">7th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="01/11/2014" style="color:#4f4f4f">8th</a></li>
          <li style="color: #f1d02e"><a href="index.html#0" data-date="02/12/2014" style="color:#4f4f4f">9th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="05/01/2015" style="color:#4f4f4f">10th</a></li>
          <li style="color: #f1d02e"><a href="index.html#0" data-date="30/01/2015" style="color:#4f4f4f">11th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="29/02/2015" style="color:#4f4f4f">12th</a></li>
        </ol>

        <span class="filling-line" aria-hidden="true"></span>
      </div>
    </div>

    <ul class="cd-timeline-navigation" style="color:#f1d02e;">
      <li><a href="index.html#0" class="prev inactive">Prev</a></li>
      <li><a href="index.html#0" class="next">Next</a></li>
    </ul>
  </div>

  <div class="events-content">
    <ol style="color:#f1d02e;padding-left: 0px">
      <li class="selected" data-date="16/01/2014">
        <em style="display: none">January 16th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Membuka laman web osd-hmpipaum.com
        </p>
      </li>

      <li data-date="28/02/2014">
        <em style="display: none">February 28th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Memilih page olimpiade</a>”.
        </p>
      </li>

      <li data-date="20/04/2014">
        <em style="display: none">March 20th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Memilih pendaftaran untuk pelaksanaan online atau offline
        </p>
      </li>

      <li data-date="20/05/2014">
        <em style="display: none">May 20th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Menentukan rayon yang diinginkan untuk pelaksanaan olimpiade online atau offline</b>
        </p>
      </li>

      <li data-date="09/07/2014">
        <em style="display: none">July 9th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Mengisi formulir pendaftaran pada laman tersebut
        </p>
      </li>

      <li data-date="30/08/2014">
        <em style="display: none">August 30th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Mengupload kartu tanda pelajar dan foto resmi 4x6
        </p>
      </li>

      <li data-date="15/09/2014">
        <em style="display: none">September 15th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Menyimpan data
        </p>
      </li>

      <li data-date="01/11/2014">
        <em style="display: none">November 1st, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Melakukan pembayaran melalui rekening bank BNI 418372136 a.n HABIBAH
          LUTFIANI
        </p>
      </li>

      <li data-date="02/12/2014">
        <em style="display: none">November 1st, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Menunggu accept pembayaran oleh panitia dalam jangka waktu 1x24 jam
        </p>
      </li>

      <li data-date="05/01/2015">
        <em style="display: none">November 1st, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Jika sudah di accept oleh panitia, kemudian membuka kembali laman osd-
          hmpipaum.com untuk mengupload bukti pembayaran. Jika belum di accept dalam
          jangka waktu maksimal 1x24 jam, silahkan menghubungi panitia pada CP yang
          sudah tertera
        </p>
      </li>

      <li data-date="30/01/2015">
        <em style="display: none">November 1st, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Mengunci data formulir pendaftaran online. Langkah ini merupakan tahap akhir
bagi peserta yang mengikuti babak penyisihan secara online. (Jika sudah di kunci,
data tidak dapat dirubah kembali)
        </p>
      </li>

      <li data-date="29/02/2015">
        <em style="display: none">November 1st, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Mendownload dan mencetak bukti pendaftaran (kartu peserta) bagi peserta yang
mengikuti babak penyisihan secara offline untuk diserahkan saat registrasi ulang
babak penyisihan offline pada masing-masing rayon
        </p>
      </li>
    </ol>
  </div>
</section>
<br><br>
<h3 class="text-center text-uppercase text-secondary mb-0" style="color:#009b4c!important;padding-bottom: 10px">Offline</h3>
        <section class="cd-horizontal-timeline" style="margin-top: 0px;margin-bottom: 0px;padding-top: 0px;padding-bottom: 0px" data-aos="fade-down">
  <div class="timeline">
    <div class="events-wrapper">
      <div class="events">
        <ol>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="16/01/2014" class="selected" style="color:#4f4f4f">1st</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="28/02/2014" style="color:#4f4f4f">2nd</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="20/04/2014" style="color:#4f4f4f">3rd</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="20/05/2014" style="color:#4f4f4f">4th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="09/07/2014" style="color:#4f4f4f">5th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="30/08/2014" style="color:#4f4f4f">6th</a></li>
          <li style="color:#f1d02e"><a href="index.html#0" data-date="29/09/2014" style="color:#4f4f4f">7th</a></li>
        </ol>

        <span class="filling-line" aria-hidden="true"></span>
      </div>
    </div>

    <ul class="cd-timeline-navigation" style="color:#f1d02e;">
      <li><a href="index.html#0" class="prev inactive">Prev</a></li>
      <li><a href="index.html#0" class="next">Next</a></li>
    </ul>
  </div>

  <div class="events-content">
    <ol style="color:#f1d02e;padding-left: 0px">
      <li class="selected" data-date="16/01/2014">
        <em style="display: none">January 16th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Mendatangi kesekretariatan HMP IPA Gedung O8 lantai 1 Universitas Negeri
Malang
        </p>
      </li>

      <li data-date="28/02/2014">
        <em style="display: none">February 28th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Mengisi formulir pendaftaran yang telah disediakan oleh panitia dengan lengkap
dan benar
        </p>
      </li>

      <li data-date="20/04/2014">
        <em style="display: none">March 20th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Menyerahkan formulir pendaftaran yang telah diisi
        </p>
      </li>

      <li data-date="20/05/2014">
        <em style="display: none">May 20th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Menyerahkan fotokopi kartu pelajar sebanyak 2 lembar
        </p>
      </li>

      <li data-date="09/07/2014">
        <em style="display: none">July 9th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Menyerahkan pas foto 4x6 resmi sebanyak 2 lembar
        </p>
      </li>

      <li data-date="30/08/2014">
        <em style="display: none">August 30th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Melakukan pembayaran sesuai dengan ketentuan:<br>
          Gelombang 1: 11 Juni - 31 Juli 2018 = Rp. 65.000<br>
          Gelombang 2: 1 Agustus - 15 September 2018 = Rp. 70.000<br>
          Gelombang 3: 16 September – 2 Oktober 2018 = Rp. 75.000<br>
          On The Spot : 7 Oktober 2018 = Rp. 85.000
        </p>
      </li>

      <li data-date="29/09/2014">
        <em style="display: none">August 30th, 2014</em>
        <p style="color:#4f4f4f;font-size: 1rem;text-align: center;">
          Pendaftaran dapat dilakukan pada pukul 10.00 – 15.00 WIB pada hari Senin-
          Jum’at. Untuk peserta di luar Malang dapat melakukan pembayaran di panitia
          HMP IPA saat melakukan publikasi
        </p>
      </li>
    </ol>
  </div>
</section>
      </div>
    </section>

    <section class="portfolio" id="gallery">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Galeri</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
           <div class="row">
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-1" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #008fd7">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g1_1.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-2" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #008fd7">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g2_1.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
               <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-3" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #008fd7">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g3_1.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-4" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #008fd7">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g4_1.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-5" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #008fd7">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g5_1.jpg">
              </a>
            </div>
            <div class="col-md-6 col-lg-4">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#portfolio-modal-6" data-aos="fade-in">
                <div class="portfolio-item-caption d-flex position-absolute h-100 w-100" style="background-color: #008fd7">
                  <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                    <i class="fa fa-search-plus fa-3x"></i>
                  </div>
                </div>
                <img class="img-fluid" src="assets/img/g6_1.jpg">
              </a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-lg-2">
            </div>
      <div class="col-md-6 col-lg-2">
            </div>
          </div>
        </div>
      </section>
  

    <section class="bg-primary text-white mb-0" style="background-color: #009b4c!important" id="region">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">Region</h2>
        <hr class="star-light mb-5">
        <div class="row text-center">

            <div class="col-lg-4 col-sm-6 py-2">
                <a>
                  <img class="icon-5x" src="assets/img/portfolio/5x.png" style="width: 141px;height: 141px" alt="Malang" data-aos="zoom-out-up">
                </a>
                <h4 class="mt-2 txt-sci" style="color:#f1d02e">
                  Malang
                </h4>
                <p><strong>Lokasi Penyisihan</strong>:<br>Universitas Negeri Malang & Online
                </p>
              </div>

      <div class="col-lg-4 col-sm-6 py-2">
        <a>
          <img class="icon-5x" src="assets/img/portfolio/1x.png" style="width: 141px;height: 141px" alt="Surabaya" data-aos="fade-up">
        </a>
        <h4 class="mt-2 txt-edu" style="color:#f1d02e">
          Surabaya
        </h4>
        <p>
          <strong>Lokasi Penyisihan</strong>:<br>SMPN 3 Surabaya & Online
        </p>
      </div>

      <div class="col-lg-4 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/6x.png" style="width: 141px;height: 141px" alt="Kediri" data-aos="zoom-out-up">
          </a>
          <h4 class="mt-2 txt-creative" style="color:#f1d02e">
            Jombang
          </h4>
          <p><strong>Lokasi Penyisihan</strong>:<br>SMPN 1 Jombang & Online
          </p>
        </div>



      <div class="col-lg-4 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/7x.png" style="width: 141px;height: 141px" alt="Gresik" data-aos="fade-up">
          </a>
          <h4 class="mt-2 txt-edu" style="color:#f1d02e">
            Lamongan
          </h4>
          <p><strong>Lokasi Penyisihan</strong>:<br>SMPN 2 Lamongan & Online
          </p>
        </div>
  

     
      <div class="col-lg-4 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/8x.png" style="width: 141px;height: 141px" alt="Tuban" data-aos="zoom-out-up">
          </a>
          <h4 class="mt-2 txt-urban" style="color:#f1d02e">
            Banyuwangi
          </h4>
          <p><strong>Lokasi Penyisihan</strong>:<br>SMPN 1 Genteng & Online
          </p>
        </div>
  
        <div class="col-lg-4 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/9x.png" style="width: 141px;height: 141px" alt="Padang" data-aos="fade-up">
          </a>
          <h4 class="mt-2 txt-tourism" style="color:#f1d02e">
            Ponorogo
          </h4>
          <p><strong>Lokasi Penyisihan</strong>:<br>SMPN 6 Ponorogo & Online
          </p>
        </div>
  
        <div class="col-lg-4 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/10x.png" style="width: 141px;height: 141px" alt="Pekanbaru" data-aos="fade-in">
          </a>
          <h4 class="mt-2 txt-digital" style="color:#f1d02e">
            Tulungagung
          </h4>
          <p><strong>Lokasi Penyisihan</strong>:<br>SMPN 2 Tulungagung & Online
          </p>
        </div>



      <div class="col-lg-4 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/11x.png" style="width: 141px;height: 141px" alt="Jember" data-aos="zoom-out">
          </a>
          <h4 class="mt-2 txt-sci" style="color:#f1d02e">
            Pamekasan
          </h4>
          <p><strong>Lokasi Penyisihan</strong>:<br>SMPN 1 Pamekasan & Online
          </p>
        </div>

      <div class="col-lg-4 col-sm-6 py-2">
        <a>
          <img class="icon-5x" src="assets/img/portfolio/2x_1.png" style="width: 141px;height: 141px" alt="Jakarta" data-aos="fade-in">
        </a>
        <h4 class="mt-2 txt-urban" style="color:#f1d02e">
          Bandung
        </h4>
        <p>
          <strong>Lokasi Penyisihan</strong>:<br>Online
        </p>
      </div>

      <div class="col-lg-4 col-sm-6 py-2">
        <a>
          <img class="icon-5x" src="assets/img/portfolio/3x.png" style="width: 141px;height: 141px" alt="Banten" data-aos="zoom-out">
        </a>
        <h4 class="mt-2 txt-tourism" style="color:#f1d02e">
          Banten
        </h4>
        <p><strong>Lokasi Penyisihan</strong>:<br>Online
        </p>
      </div>

      <div class="col-lg-4 col-sm-6 py-2">
        <a>
          <img class="icon-5x" src="assets/img/portfolio/4x.png" style="width: 141px;height: 141px" alt="Yogyakarta" data-aos="fade-in">
        </a>
        <h4 class="mt-2 txt-digital" style="color:#f1d02e">
          Yogyakarta
        </h4>
        <p>Lokasi Penyisihan</strong>:<br>Online
        </p>
      </div>


      <div class="col-lg-4 col-sm-6 py-2">
        <a>
          <img class="icon-5x" src="assets/img/portfolio/12x_1.png" style="width: 141px;height: 141px" alt="Madiun" data-aos="zoom-out">
        </a>
        <h4 class="mt-2 txt-creative" style="color:#f1d02e">
          Bali
        </h4>
        <p><strong>Lokasi Penyisihan</strong>:<br>Online
        </p>
      </div>

      <div class="col-lg-6 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/13x.png" style="width: 141px;height: 141px" alt="Yogyakarta" data-aos="fade-in">
          </a>
          <h4 class="mt-2 txt-digital" style="color:#f1d02e">
            Semarang
          </h4>
          <p>Lokasi Penyisihan</strong>:<br>Online
          </p>
        </div>
  
  
        <div class="col-lg-6 col-sm-6 py-2">
          <a>
            <img class="icon-5x" src="assets/img/portfolio/14x.png" style="width: 141px;height: 141px" alt="Madiun" data-aos="zoom-out">
          </a>
          <h4 class="mt-2 txt-creative" style="color:#f1d02e">
            Tegal
          </h4>
          <p><strong>Lokasi Penyisihan</strong>:<br>Online
          </p>
        </div>

    </div>
  </div>
    </section>

    <section class="portfolio" id="sponsor">
        <div class="container">
          <h2 class="text-center text-uppercase text-secondary mb-0" style="color:#4f4f4f!important">Didukung Oleh</h2>
          <hr class="star-dark mb-5" style="border-color: #4f4f4f">
          <div class="row">
            <div class="col-md-12 col-lg-12" style="margin-top: 10px">
              <a class="portfolio-item d-block mx-auto" onclick="return false" href="#">
                <center><img class="img-fluid" src="assets/img/gabungan-logo_1.png" style="max-width: 100%" data-aos="zoom-out"></center>
              </a>
            </div>
            <div class="col-md-6 col-lg-1" style="margin-top: 10px"></div>
          </div>
        </div>
      </section>

      <section class="portfolio" style="padding-top:0px">
          <div class="container">
          <div id="disqus_thread"></div>
          <script>
          /**
          *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
          *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
          /*
          var disqus_config = function () {
          this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
          this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
          };
          */
          (function() { // DON'T EDIT BELOW THIS LINE
          var d = document, s = d.createElement('script');
          s.src = 'https://olimpiade-ipa-um.disqus.com/embed.js';
          s.setAttribute('data-timestamp', +new Date());
          (d.head || d.body).appendChild(s);
          })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
        </div>                         
      </section>
      

    <footer class="footer text-center" style="background-color: #f1d02e">
      <div class="container">
        <div class="row">
          <div class="col-md-6 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Kontak</h4>
            <p class="lead mb-0">085230669149 (Via)
              <br>085648008741 (Yola)
              <br>085733262798 (Dinas)</p>
          </div>
          <div class="col-md-6 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Sosial Media</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" style="border-width:0.15rem" href="https://www.instagram.com/hmpipa_um/">
                  <i class="fa fa-fw fa-instagram"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" style="border-width:0.15rem" href="https://www.instagram.com/osd_hmpipaum2018/">
                  <i class="fa fa-fw fa-instagram"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <div class="copyright py-4 text-center text-white" style="background-color: #008fd7">
      <div class="container">
        <small>Made with <b style="color:red">&hearts;</b> by <a href="http://olimpiade.id" style="color:black"><b>Olimpiade.id</b></a></small>
      </div>
    </div>

    <div class="scroll-to-top position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

    <!-- Modal 1 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-1">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#009b4c"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g1_1.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #009b4c;border-color: #009b4c">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-2">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#009b4c"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g2_1.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #009b4c;border-color: #009b4c">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 3 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-3">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#009b4c"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g3_1.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #009b4c;border-color: #009b4c">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 4 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-4">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#009b4c"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g4_1.jpg" alt="">
              <p class="mb-5"></p>
             <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #009b4c;border-color: #009b4c">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 5 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-5">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#009b4c"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g5_1.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #009b4c;border-color: #009b4c">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 6 -->
    <div class="portfolio-modal mfp-hide" id="portfolio-modal-6">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="index.html#">
          <i class="fa fa-3x fa-times" style="color:#009b4c"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0" style="color: #4f4f4f!important">Gallery</h2>
              <hr class="star-dark mb-5" style="border-color: #4f4f4f">
              <img class="img-fluid mb-5" src="assets/img/g6_1.jpg" alt="">
              <p class="mb-5"></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="index.html#" style="background-color: #009b4c;border-color: #009b4c">
                <i class="fa fa-close"></i>
                Close</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      function getTimeRemaining(endtime) {
      var t = Date.parse(endtime) - Date.parse(new Date());
      var seconds = Math.floor((t / 1000) % 60);
      var minutes = Math.floor((t / 1000 / 60) % 60);
      var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
      var days = Math.floor(t / (1000 * 60 * 60 * 24));
      return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
      };
    }

    function initializeClock(id, endtime) {
      var clock = document.getElementById(id);
      var daysSpan = clock.querySelector('.days');
      var hoursSpan = clock.querySelector('.hours');
      var minutesSpan = clock.querySelector('.minutes');
      var secondsSpan = clock.querySelector('.seconds');

      function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
          clearInterval(timeinterval);
        }
      }

      updateClock();
      var timeinterval = setInterval(updateClock, 1000);
    }

    var deadline = new Date(Date.parse(new Date("Oct 7, 2018 23:59:00")));
    initializeClock('clockdiv', deadline);
    </script>

    <script src="assets/vendor/jquery/jquery.min.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <script src="assets/js/jqBootstrapValidation.js"></script>
    <script src="assets/js/contact_me.js"></script>
    <script src="assets/js/aos.js"></script>

    <script src="assets/js/freelancer.min.js"></script>
    <script src="assets/js/modernizr.js"></script>
    <script src="assets/js/main.js"></script>

    <script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
    <script type="text/javascript">
   jQuery(document).ready(function($){
  var timelines = $('.cd-horizontal-timeline'),
    eventsMinDistance = 60;

  (timelines.length > 0) && initTimeline(timelines);

  function initTimeline(timelines) {
    timelines.each(function(){
      var timeline = $(this),
        timelineComponents = {};

      timelineComponents['timelineWrapper'] = timeline.find('.events-wrapper');
      timelineComponents['eventsWrapper'] = timelineComponents['timelineWrapper'].children('.events');
      timelineComponents['fillingLine'] = timelineComponents['eventsWrapper'].children('.filling-line');
      timelineComponents['timelineEvents'] = timelineComponents['eventsWrapper'].find('a');
      timelineComponents['timelineDates'] = parseDate(timelineComponents['timelineEvents']);
      timelineComponents['eventsMinLapse'] = minLapse(timelineComponents['timelineDates']);
      timelineComponents['timelineNavigation'] = timeline.find('.cd-timeline-navigation');
      timelineComponents['eventsContent'] = timeline.children('.events-content');

      setDatePosition(timelineComponents, eventsMinDistance);

      var timelineTotWidth = setTimelineWidth(timelineComponents, eventsMinDistance);

      timeline.addClass('loaded');

      timelineComponents['timelineNavigation'].on('click', '.next', function(event){
        event.preventDefault();
        updateSlide(timelineComponents, timelineTotWidth, 'next');
      });

      timelineComponents['timelineNavigation'].on('click', '.prev', function(event){
        event.preventDefault();
        updateSlide(timelineComponents, timelineTotWidth, 'prev');
      });

      timelineComponents['eventsWrapper'].on('click', 'a', function(event){
        event.preventDefault();
        timelineComponents['timelineEvents'].removeClass('selected');
        $(this).addClass('selected');
        updateOlderEvents($(this));
        updateFilling($(this), timelineComponents['fillingLine'], timelineTotWidth);
        updateVisibleContent($(this), timelineComponents['eventsContent']);
      });

      timelineComponents['eventsContent'].on('swipeleft', function(){
        var mq = checkMQ();
        ( mq == 'mobile' ) && showNewContent(timelineComponents, timelineTotWidth, 'next');
      });
      timelineComponents['eventsContent'].on('swiperight', function(){
        var mq = checkMQ();
        ( mq == 'mobile' ) && showNewContent(timelineComponents, timelineTotWidth, 'prev');
      });

      $(document).keyup(function(event){
        if(event.which=='37' && elementInViewport(timeline.get(0)) ) {
          showNewContent(timelineComponents, timelineTotWidth, 'prev');
        } else if( event.which=='39' && elementInViewport(timeline.get(0))) {
          showNewContent(timelineComponents, timelineTotWidth, 'next');
        }
      });
    });
  }

  function updateSlide(timelineComponents, timelineTotWidth, string) {

    var translateValue = getTranslateValue(timelineComponents['eventsWrapper']),
      wrapperWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', ''));

    (string == 'next')
      ? translateTimeline(timelineComponents, translateValue - wrapperWidth + eventsMinDistance, wrapperWidth - timelineTotWidth)
      : translateTimeline(timelineComponents, translateValue + wrapperWidth - eventsMinDistance);
  }

  function showNewContent(timelineComponents, timelineTotWidth, string) {

    var visibleContent =  timelineComponents['eventsContent'].find('.selected'),
      newContent = ( string == 'next' ) ? visibleContent.next() : visibleContent.prev();

    if ( newContent.length > 0 ) {
      var selectedDate = timelineComponents['eventsWrapper'].find('.selected'),
        newEvent = ( string == 'next' ) ? selectedDate.parent('li').next('li').children('a') : selectedDate.parent('li').prev('li').children('a');

      updateFilling(newEvent, timelineComponents['fillingLine'], timelineTotWidth);
      updateVisibleContent(newEvent, timelineComponents['eventsContent']);
      newEvent.addClass('selected');
      selectedDate.removeClass('selected');
      updateOlderEvents(newEvent);
      updateTimelinePosition(string, newEvent, timelineComponents, timelineTotWidth);
    }
  }

  function updateTimelinePosition(string, event, timelineComponents, timelineTotWidth) {

    var eventStyle = window.getComputedStyle(event.get(0), null),
      eventLeft = Number(eventStyle.getPropertyValue("left").replace('px', '')),
      timelineWidth = Number(timelineComponents['timelineWrapper'].css('width').replace('px', '')),
      timelineTotWidth = Number(timelineComponents['eventsWrapper'].css('width').replace('px', ''));
    var timelineTranslate = getTranslateValue(timelineComponents['eventsWrapper']);

        if( (string == 'next' && eventLeft > timelineWidth - timelineTranslate) || (string == 'prev' && eventLeft < - timelineTranslate) ) {
          translateTimeline(timelineComponents, - eventLeft + timelineWidth/2, timelineWidth - timelineTotWidth);
        }
  }

  function translateTimeline(timelineComponents, value, totWidth) {
    var eventsWrapper = timelineComponents['eventsWrapper'].get(0);
    value = (value > 0) ? 0 : value;
    value = ( !(typeof totWidth === 'undefined') &&  value < totWidth ) ? totWidth : value;
    setTransformValue(eventsWrapper, 'translateX', value+'px');

    (value == 0 ) ? timelineComponents['timelineNavigation'].find('.prev').addClass('inactive') : timelineComponents['timelineNavigation'].find('.prev').removeClass('inactive');
    (value == totWidth ) ? timelineComponents['timelineNavigation'].find('.next').addClass('inactive') : timelineComponents['timelineNavigation'].find('.next').removeClass('inactive');
  }

  function updateFilling(selectedEvent, filling, totWidth) {

    var eventStyle = window.getComputedStyle(selectedEvent.get(0), null),
      eventLeft = eventStyle.getPropertyValue("left"),
      eventWidth = eventStyle.getPropertyValue("width");
    eventLeft = Number(eventLeft.replace('px', '')) + Number(eventWidth.replace('px', ''))/2;
    var scaleValue = eventLeft/totWidth;
    setTransformValue(filling.get(0), 'scaleX', scaleValue);
  }

  function setDatePosition(timelineComponents, min) {
    for (i = 0; i < timelineComponents['timelineDates'].length; i++) {
        var distance = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][i]),
          distanceNorm = Math.round(distance/timelineComponents['eventsMinLapse']) + 2;
        timelineComponents['timelineEvents'].eq(i).css('left', distanceNorm*min+'px');
    }
  }

  function setTimelineWidth(timelineComponents, width) {
    var timeSpan = daydiff(timelineComponents['timelineDates'][0], timelineComponents['timelineDates'][timelineComponents['timelineDates'].length-1]),
      timeSpanNorm = timeSpan/timelineComponents['eventsMinLapse'],
      timeSpanNorm = Math.round(timeSpanNorm) + 4,
      totalWidth = timeSpanNorm*width;
    timelineComponents['eventsWrapper'].css('width', totalWidth+'px');
    updateFilling(timelineComponents['timelineEvents'].eq(0), timelineComponents['fillingLine'], totalWidth);

    return totalWidth;
  }

  function updateVisibleContent(event, eventsContent) {
    var eventDate = event.data('date'),
      visibleContent = eventsContent.find('.selected'),
      selectedContent = eventsContent.find('[data-date="'+ eventDate +'"]'),
      selectedContentHeight = selectedContent.height();

    if (selectedContent.index() > visibleContent.index()) {
      var classEnetering = 'selected enter-right',
        classLeaving = 'leave-left';
    } else {
      var classEnetering = 'selected enter-left',
        classLeaving = 'leave-right';
    }

    selectedContent.attr('class', classEnetering);
    visibleContent.attr('class', classLeaving).one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
      visibleContent.removeClass('leave-right leave-left');
      selectedContent.removeClass('enter-left enter-right');
    });
    eventsContent.css('height', selectedContentHeight+'px');
  }

  function updateOlderEvents(event) {
    event.parent('li').prevAll('li').children('a').addClass('older-event').end().end().nextAll('li').children('a').removeClass('older-event');
  }

  function getTranslateValue(timeline) {
    var timelineStyle = window.getComputedStyle(timeline.get(0), null),
      timelineTranslate = timelineStyle.getPropertyValue("-webkit-transform") ||
            timelineStyle.getPropertyValue("-moz-transform") ||
            timelineStyle.getPropertyValue("-ms-transform") ||
            timelineStyle.getPropertyValue("-o-transform") ||
            timelineStyle.getPropertyValue("transform");

        if( timelineTranslate.indexOf('(') >=0 ) {
          var timelineTranslate = timelineTranslate.split('(')[1];
        timelineTranslate = timelineTranslate.split(')')[0];
        timelineTranslate = timelineTranslate.split(',');
        var translateValue = timelineTranslate[4];
        } else {
          var translateValue = 0;
        }

        return Number(translateValue);
  }

  function setTransformValue(element, property, value) {
    element.style["-webkit-transform"] = property+"("+value+")";
    element.style["-moz-transform"] = property+"("+value+")";
    element.style["-ms-transform"] = property+"("+value+")";
    element.style["-o-transform"] = property+"("+value+")";
    element.style["transform"] = property+"("+value+")";
  }

  function parseDate(events) {
    var dateArrays = [];
    events.each(function(){
      var dateComp = $(this).data('date').split('/'),
        newDate = new Date(dateComp[2], dateComp[1]-1, dateComp[0]);
      dateArrays.push(newDate);
    });
      return dateArrays;
  }

  function parseDate2(events) {
    var dateArrays = [];
    events.each(function(){
      var singleDate = $(this),
        dateComp = singleDate.data('date').split('T');
      if( dateComp.length > 1 ) {
        var dayComp = dateComp[0].split('/'),
          timeComp = dateComp[1].split(':');
      } else if( dateComp[0].indexOf(':') >=0 ) {
        var dayComp = ["2000", "0", "0"],
          timeComp = dateComp[0].split(':');
      } else {
        var dayComp = dateComp[0].split('/'),
          timeComp = ["0", "0"];
      }
      var newDate = new Date(dayComp[2], dayComp[1]-1, dayComp[0], timeComp[0], timeComp[1]);
      dateArrays.push(newDate);
    });
      return dateArrays;
  }

  function daydiff(first, second) {
      return Math.round((second-first));
  }

  function minLapse(dates) {
    var dateDistances = [];
    for (i = 1; i < dates.length; i++) {
        var distance = daydiff(dates[i-1], dates[i]);
        dateDistances.push(distance);
    }
    return Math.min.apply(null, dateDistances);
  }

  function elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;

    while(el.offsetParent) {
        el = el.offsetParent;
        top += el.offsetTop;
        left += el.offsetLeft;
    }

    return (
        top < (window.pageYOffset + window.innerHeight) &&
        left < (window.pageXOffset + window.innerWidth) &&
        (top + height) > window.pageYOffset &&
        (left + width) > window.pageXOffset
    );
  }

  function checkMQ() {
    return window.getComputedStyle(document.querySelector('.cd-horizontal-timeline'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
  }
});
    </script>
    <script type="text/javascript">
            $(document).ready(function(){
                $("#newsModal").modal('show');
            });
        </script>
        <!-- News Modal -->
        <div id="newsModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pengumuman !!!</h4>
                </div>
                <div class="modal-body">
                    <h6>Selamat !!!<br>
                      
                      Klik link dibawah ini untuk melihat pengumuman babak penyisihan<br>

                      <a href="https://drive.google.com/file/d/1wILucXNTd0WAFPrQWIyJiUEKyKFeaskR/view">Pengumuman</a>
                    </h6>              
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                </div>
                </div>

            </div>
        </div>    <!--  -->

  </body>

</html>
http://ujian.osd-hmpipaum.com/login