<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index()
    {
        return view('blog/home');
    }

    public function show($id)
    {
        $nilai = 'ini adalah linknya ' . $id;
        //$users = DB::table('users')->where('username', 'like', '%a%')->get();
        /*$users = DB::table('users')->insert(
            ['username' => 'testing', 'password' => '123asd']
        );*/
        /*DB::table('users')->where('username','alvian')->update(
            ['username' => 'al']
        );*/
        DB::table('users')->where('id','>','2')->delete();
        $users = DB::table('users')->get();
        $unescaped = '<b> alert("x!") </b>';

        return view('blog/single', ['blog' => $nilai, 'users' => $users, 'unescaped' => $unescaped]);
    }
}
